import {Link} from 'react-router-dom'


export function Header() {
    return (

        <div className='nav'>

        
            <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li className="nav-item" role="presentation">
                <Link className="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true" to=''>Home</Link>
                </li>
                <li className="nav-item" role="presentation">
                <Link className="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false" to='#'>Profile</Link>
                </li>
                <li className="nav-item" role="presentation">
                <Link className="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false" to=''>Contact</Link>
                </li>
            </ul>
          
        </div>


     



       
    );

}